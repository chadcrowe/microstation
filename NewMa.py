from os import listdir
import os
from os.path import join
import shutil
import itertools
baseFiles = r"C:\Users\CCrowe\Documents\MDLProjects V8i (SELECTseries 3)"

def getExistingFolders():
    folders = []
    for folder in listdir(baseFiles):
        folders.append(folder)
    return folders

existingFolders = getExistingFolders()
folderName = input("Please Enter the Base Program's Name\n")

def getBaseFolder():
    for folder in listdir(baseFiles):
        if folder == folderName:
            baseFolder = os.path.join(baseFiles,folder)
            return baseFolder
def getAllFilesToBeReplaced():
    files = []
    for file in listdir(baseFolder):
        if file.find(".") == -1:
            pathDepth2 = os.path.join(baseFolder,file)
            for fileDepth2 in listdir(pathDepth2):
                files.append(os.path.join(baseFolder,pathDepth2,fileDepth2))
        else:
            files.append(os.path.join(baseFolder,file))
    return files
 
def copyFile(src, dest):
    try:
        shutil.copy(src, dest)
    # eg. src and dest are the same file
    except shutil.Error as e:
        print('Error: %s' % e)
    # eg. source or destination doesn't exist
    except IOError as e:
        print('Error: %s' % e.strerror)
        
baseFolder = getBaseFolder()
files = getAllFilesToBeReplaced()
newString = folderName
while newString in existingFolders:
    newString = input("Please Enter New Program Name\n")

newFolderPath = os.path.join(baseFiles,newString)

#Now Create New Folder
def getFolders(path):
	files = []
	for file in listdir(path):
		file_path = os.path.join(path,file)
		if os.path.isdir(file_path):
			files.append(file_path)
			for x in getFolders(file_path):
				files.append(x)
	return(files)
    
folders = getFolders(baseFolder)


def copy_and_create_new_files_and_folders():
    new_files = []
    os.makedirs(newFolderPath)
    for folder in folders:
        os.makedirs(folder.replace(folderName,newString))
    for file in files:
        copyFile(file,file.replace(folderName,newString))
        new_files.append(file.replace(folderName,newString))
    return(new_files)

new_files = copy_and_create_new_files_and_folders()
def cc(s):
    return (''.join(t) for t in itertools.product(*zip(s.lower(), s.upper())))
	    
for file in new_files:
    opened_file = open(file,'r')
    file_contents = opened_file.read()
    all_combos = list(cc(folderName))
    new_file_contents = file_contents #this will change in the next for loop
    for combo in all_combos:
        new_file_contents = new_file_contents.replace(combo,newString)
    opened_file.close()
    write_new_file = open(file,'w')
    write_new_file.write(new_file_contents)
    write_new_file.close()


