Attribute VB_Name = "RotateTextNode"

Sub Rotate_TextNodes()
Dim v As Variant
Dim typ As Variant
Dim ee As ElementEnumerator
Dim e As Element
Dim line As LineElement
Dim textnode As TextNodeElement
Dim textele As TextElement
Dim cline As ComplexStringElement
Dim ii As Double
Dim mat As Matrix3d
Set ee = ActiveModelReference.Scan()
While ee.MoveNext
    Set e = ee.Current()
    If e.IsGraphical Then
        If e.IsHighlighted Then
            If e.IsTextNodeElement Then
                Set textnode = e
                CopyElementNoRotation textnode
            End If
        End If
    End If
Wend
End Sub
Function GetAngleDifference(ByRef textnode As TextNodeElement)
Dim oCell As CellElement
Dim rotationX As Double
Dim rotationY As Double
Dim rotationZ As Double
Dim scaleFactor As Double
If (Matrix3dIsXRotationYRotationZRotationScale( _
    textnode.rotation, _
    rotationX, _
    rotationY, _
    rotationZ, _
    scaleFactor)) Then
        GetAngleDifference = -rotationZ
End If
End Function
Function Move_text_elements(ByRef textnode As TextNodeElement)
Dim textele As TextElement
Dim rotation As Matrix3d
Dim originalLocation As Point3d
originalLocation = textnode.origin
Set ee2 = textnode.GetSubElements
rotation = textnode.rotation
rotation = Matrix3dInverse(rotation)
Angle = GetAngleDifference(textnode)
textnode.RotateAboutZ textnode.origin, Angle
textnode.Rewrite
MoveToOrigin textnode, originalLocation
End Function
Function MoveToOrigin(ByRef textnode As TextNodeElement, ByRef origin As Point3d)
Dim mover As Point3d
mover.X = origin.X - textnode.origin.X
mover.Y = origin.Y - textnode.origin.Y

textnode.Move mover
textnode.Rewrite
End Function
Function CopyElementNoRotation(ByRef textnode As TextNodeElement)
Dim mver As Point3d
Dim tn As TextNodeElement
Dim textele As TextElement
Dim currentStyle As TextStyle
Move_text_elements textnode

End Function

