﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Attachments;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;
using Scanners;
using Models;
using Scanners.Element.Highlighted;

namespace CopyTitleToEveryPage
{
    internal sealed class MyAddin : Bentley.MicroStation.AddIn
    {
        private MyAddin(System.IntPtr mdlDesc)
            : base(mdlDesc)
        {
        }

        protected override int Run(string[] commandLine)
        {
            IGetActiveScannable models = new ModelAttachment();
            models.GetScannablesWithinModel();
            IFindHighlightedElements finder = new GetHighlightedElements(models);
            finder.RecursivelySearchForHighlightedElements();
            List<BNS.Element> elements = finder.GetHighlightedElementList();
            BNS.Application app = Utilities.ComApp;
            foreach (ModelReference model in app.ActiveDesignFile.Models)
            {
                string name = model.Name;
                string replacedName = name.Replace("78029115J", "");
                int value;
                if (int.TryParse(replacedName, out value))
                {
                    foreach (Element element in elements)
                    {
                        app.ActiveDesignFile.Models[model.Name].AddElement(element);
                        var type = element.Type;
                        if (element.IsTextNodeElement())
                        {
                            ElementEnumerator tnEe = element.AsTextNodeElement().GetSubElements();
                            while (tnEe.MoveNext())
                            {
                                Element tnElement = tnEe.Current;
                                if (tnElement.IsTextElement())
                                {
                                    TextElement textElement = tnElement.AsTextElement();
                                    textElement.Text = "J." + value.ToString();
                                    textElement.Rewrite();
                                }
                            }
                        }
                    }
                }
            }
            return 0;
        }
    }
}