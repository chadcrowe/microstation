#pragma once  

#ifdef MODEL_API_EXPORTS
#define MODEL_API __declspec(dllexport)   
#else  
#define MODEL_API __declspec(dllimport)   
#endif  

namespace modelLibrary
{
	class ModelIterator{
	public:
		static MODEL_API void IterateModels();
	};
}
