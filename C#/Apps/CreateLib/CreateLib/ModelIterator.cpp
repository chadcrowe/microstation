#define winNT
#include <mdl.h>
#include <msdgnobj.fdf>
#include <msmodel.fdf>
#include <msdgnmodelref.fdf>
#include <modelindex.fdf>
#include <msdgnobj.fdf>
#include <msdgnmodelref.fdf>

//needed to unload
#include    <cmdlist.h>
#include    <dlogman.fdf>
#include    <mssystem.fdf>
#include "ModelIterator.h"

#include <mdl.h>
#include <msdgnobj.fdf>
#include <msmodel.fdf>
#include <msdgnmodelref.fdf>
#include <modelindex.fdf>
#include <msdgnobj.fdf>
#include <msdgnmodelref.fdf>

namespace modelLibrary
{
	void ModelIterator::IterateModels(){
		DgnFileObjP dgnFile = mdlDgnFileObj_getMasterFile();

		DgnModelRefListP loadedModels = mdlDgnFileObj_getModelRefList(dgnFile);

		int loadedModelsCount = mdlModelRefList_getCount(loadedModels);
		for (int i = 0; i < loadedModelsCount; i++){
			DgnModelRefP modelRef = mdlModelRefList_get(loadedModels, i);
			mdlModelRef_activateAndDisplay(modelRef);
		}
	}
}




