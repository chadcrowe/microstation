﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;
using Selectors.Levels.ByName;

namespace LevelStringHighlightor
{
    internal sealed class MyAddin : Bentley.MicroStation.AddIn
    {
        private MyAddin(System.IntPtr mdlDesc)
            : base(mdlDesc)
        {
        }

        protected override int Run(string[] commandLine)
        {
            if (commandLine.Length > 2)
            {
                string levelName = "";
                if (commandLine.Length > 3)
                {
                    for (int i = 3; i < commandLine.Length; i++)
                    {
                        levelName += commandLine[i].ToString() + " ";
                    }
                    levelName = levelName.Trim();
                }
                else
                {
                    levelName = commandLine[commandLine.Length - 1];
                }
                
                LevelHighlighter highlighter = new LevelHighlighter(levelName);
                highlighter.HighlightLevels();
            }
            return 0;
        }
    }
}
