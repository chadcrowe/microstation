﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;
using Clipboard.Highlighted;
using Scanners.Element.Highlighted;
using Attachments;

namespace CopyHighlightedElementsTextToClipboard
{
    internal sealed class MyAddin : Bentley.MicroStation.AddIn
    {
        private MyAddin(System.IntPtr mdlDesc)
            : base(mdlDesc)
        {
        }
        protected override int Run(string[] commandLine)
        {
            IGetAttachments modelGetter = AttachmentGetter();
            Scanners.Element.Highlighted.IFindHighlightedElements finder = GetFinder(modelGetter);
            CopyHighlightedElement copyToClipboard = new CopyHighlightedElement(finder);
            copyToClipboard.SetClipBoardToHighlightedElementLevelString();
            return 0;
        }

        private IGetAttachments AttachmentGetter()
        {
            IGetAttachments modelGetter = new ModelAttachment(null);
            return modelGetter;
        }
        private Scanners.Element.Highlighted.IFindHighlightedElements GetFinder(IGetAttachments modelGetter)
        {
            Scanners.Element.Highlighted.IFindHighlightedElements finder = new GetHighlightedElements(modelGetter, null);
            return finder;
        }
    }
}
