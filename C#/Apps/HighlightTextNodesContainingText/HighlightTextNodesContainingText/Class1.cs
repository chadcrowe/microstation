﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;
using Scanners.TextNodeScans;
using Readers;

namespace HighlightTextNodesContainingText
{
    internal sealed class MyAddin : Bentley.MicroStation.AddIn
    {
        private MyAddin(System.IntPtr mdlDesc)
            : base(mdlDesc)
        {
        }
        protected override int Run(string[] commandLine)
        {
            if (commandLine.Length == 3)
            {
                string textIWant = commandLine[2];
                TextNodeTextScanner scanner = new TextNodeTextScanner(null,new TextNodeReader(),textIWant);
            }
            return 0;
        }
    }
}
