﻿using System;
using System.Collections.Generic;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using MSB = Bentley.Interop.MicroStationDGN;
using Scanners;
using Levels;
using System.Windows.Forms;
using Models;

namespace intermodel_transfer
{
    internal sealed class MyAddin : Bentley.MicroStation.AddIn
    {
        private MyAddin(System.IntPtr mdlDesc)
            : base(mdlDesc)
        {
        }
        protected override int Run(string[] commandLine)
        {
            MessageBox.Show("Starting");
            GetActiveDesignFileModels modelListGetter = new GetActiveDesignFileModels();
            List<ModelReference> models = modelListGetter.GetListOfModels();
            MessageBox.Show("Getting models");
            GetModelByName modelGetter = new GetModelByName(models);
            ModelReference startModel = modelGetter.GetModelFromString("ML_0050_080");
            ModelReference finalModel = modelGetter.GetModelFromString("ML_0050_E029N");
            MessageBox.Show("Got models");
            //TransferTextNodes(startModel,finalModel);


            string levelName = "phoTxtDrainage";
            GetLevelByName getLevel = new GetLevelByName();
            Level level = getLevel.GetLevelFromString(levelName);
            TextNodeScanner tnScanner = new TextNodeScanner(startModel);
            List<Element> textNodeElements = tnScanner.GetTextNodesOnLevel(level);
            PasteIntoModel paster = new PasteIntoModel(finalModel, textNodeElements);

            List<string> levelNames = new List<string> { //"phoTxtDrainage",
                "dsnTextCulvertProp",
                "dsnCulvertStrucProp",
                "conCulvert",
                "dsnCulvertStrucProp",
                "dsnTextCulvertProp",
                "phoCulvert",
                "eotCulverts",
                "eotProposedCulverts",
                "SurCulvert",
                "phoCulvert",
                "eotCulverts",
                "dsnCulvertStrucProp",
                "dsnTextCulvertProp"};
            foreach (string levName in levelNames)
            {
                getLevel = new GetLevelByName();
                level = getLevel.GetLevelFromString(levName);
                ElementScanner eScanner = new ElementScanner(startModel);
                textNodeElements = eScanner.GetElementsOnLevel(level);
                paster = new PasteIntoModel(finalModel, textNodeElements);
            }
            MessageBox.Show("Finished!");
            return 0;
        }
    }
}
