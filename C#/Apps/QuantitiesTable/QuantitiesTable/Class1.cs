﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;
using Records;

namespace QuantitiesTable
{
    internal sealed class MyAddin : Bentley.MicroStation.AddIn
    {
        private MyAddin(System.IntPtr mdlDesc)
            : base(mdlDesc)
        {
        }
        protected override int Run(string[] commandLine)
        {
            //IQuantityBuilder builder = new QuantityBuilder("STA",new List<string>(){"Begin","End"},"Length","Barrier");
            IQuantityBuilder builder = new QuantityBuilder("STA", new List<string>() { "Begin", "End","Start" }, "Length", "dsn");
            builder.Build();
            List <Quantity> quantities = builder.GetQuantities();
            IQuantityWriter writer = new QuantityExcelWriter(Utilities.ComApp.ActiveModelReference.Name,quantities);
            writer.Write();
            return 0;
        }
    }
}
