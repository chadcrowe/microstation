#pragma once
#using <ustation.dll>
#using <Bentley.General.1.0.dll>
#using <bentley.interop.microstationdgn.dll>
#using <bentley.microstation.dll>
#using <bentley.microstation.interfaces.1.0.dll>
#include  "ModelIterator.h"
#include "DgnHeaders.h"
using namespace System;
using namespace System::Windows::Forms;
using namespace ExpandPrintBorder;
ref class MyAddin sealed : public Bentley::MicroStation::AddIn
{
public:
	/*MyAddin(System::IntPtr mdlDesc)
		: base(mdlDesc)
		{
		}*/
	MyAddin(System::IntPtr mdlDesc) : Bentley::MicroStation::AddIn(mdlDesc){

	}
public:
	int Run(array<System::String^>^ commandLine) override{
		MessageBox::Show("C++/CLI application started");
		Iterate();
		return 0;
	};
	void Iterate(){
		DgnIndexIteratorP   iterator = mdlModelIterator_create(mdlModelRef_getDgnFile(ACTIVEMODEL));
		DgnIndexItemP       item = mdlModelIterator_getFirst(iterator);
		MSWChar             modelName[MAX_MODEL_NAME_LENGTH];
		while (item)
		{
			mdlModelItem_getName(item, modelName, MAX_MODEL_NAME_LENGTH);
			models_activate(modelName);

			CSharpAddin^ cs = gcnew CSharpAddin();
			cs->Main();

			item = mdlModelIterator_getNext(iterator);
		}
		mdlModelIterator_free(iterator);
		//  Active a model to enable a user to work with it
	}
	BoolInt             models_activate
		(
		MSWChar const*      modelName      //   =>	Activate this model
		)
	{
		BoolInt         activated = FALSE;
		MSWChar         activeName[MAX_MODEL_NAME_LENGTH];
		mdlModelRef_getModelName(ACTIVEMODEL, activeName);
		DgnFileObjP     dgnFileObj = mdlModelRef_getDgnFile(ACTIVEMODEL);
		DgnModelRefP    modelRef = NULL;
		if (SUCCESS == mdlModelRef_createWorkingByName(&modelRef, dgnFileObj, modelName, TRUE, FALSE)
			&&
			SUCCESS == mdlModelRef_activateAndDisplay(modelRef))
		{
			activated = models_areSame(modelRef, ACTIVEMODEL);
		}
		mdlModelRef_freeWorking(modelRef);
		return activated;
	}
	BoolInt             models_areSame
		(
		DgnModelRefP        modelRef1,
		DgnModelRefP        modelRef2
		)
	{
		return mdlModelRef_getCache(modelRef1) == mdlModelRef_getCache(modelRef2);
	}
};