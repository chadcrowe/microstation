﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;
using Fences.FencedElements;
using Levels;

namespace SelectElementsWithinFenceByLevel_App
{
    internal sealed class MyAddin : Bentley.MicroStation.AddIn
    {
        private MyAddin(System.IntPtr mdlDesc)
            : base(mdlDesc)
        {
        }
        protected override int Run(string[] commandLine)
        {
            if (commandLine.Length == 3)
            {
                string levelName = commandLine[2];
                GetLevelByName levelGetter = new GetLevelByName();
                Level level = levelGetter.GetLevelFromString(levelName);
                SelectElementsWithinFenceByLevel fenceSelector = new SelectElementsWithinFenceByLevel(level);
                fenceSelector.SelectAllElementsWithinActiveFenceByLevel();
                //TextNodeTextScanner scanner = new TextNodeTextScanner(null, new TextNodeReader(), textIWant);
            }
            return 0;
        }
    }
}
