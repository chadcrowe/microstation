﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Writers.TextNodes;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;
using Scanners.Element.Getter.Collection;
using Levels;
using Coverters.TextNode;
using Selectors.TextNodeList.MatchString;
using Readers;
using Writers.TextNodes.Style;

namespace ChangeUacTextElement_App
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    internal sealed class MyAddin : Bentley.MicroStation.AddIn
    {
        private MyAddin(System.IntPtr mdlDesc)
            : base(mdlDesc)
        {
        }
        protected override int Run(string[] commandLine)
        {
            if (commandLine.Length > 3)
            {
                MainWindow window = new MainWindow();
                GetLevelByName levelGetter = new GetLevelByName();
                Level level = levelGetter.GetLevelFromString("phoTxtDrainage");
                GetElementsByLevel getter = new GetElementsByLevel(null);
                List<BNS.Element> elementList = getter.GetterByLevel(level);
                ToTextNode converter = new ToTextNode();
                List<BNS.TextNodeElement> textNodeList = converter.Convert(elementList);
                string textToFind = commandLine[commandLine.Length - 1];
                foreach (BNS.TextNodeElement textNode in textNodeList)
                {
                    TextNodeReader tnReader = new TextNodeReader(textNode);
                    Highlighter highlighter = new Highlighter(null, tnReader, textNodeList, textToFind);
                    StyleLines changer = new StyleLines(textNode, textToFind, level);
                }
            }
            return 0;
        }
    }
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
    }
}
