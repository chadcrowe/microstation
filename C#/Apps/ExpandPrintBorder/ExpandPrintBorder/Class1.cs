﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;
using Scanners.Element.Getter.Collection;
using Attachments;
using Levels;
using Scanners;

namespace ExpandPrintBorder
{
    public class CSharpAddin
    {
        public static void EnumerateElements(List<Element> elements)
        {
            foreach (Element element in elements)
            {
                var type = element.Type;
            }
        }

        public static Element GetPlotInModelReference(ModelReference model,List<Element> bluePlotSquare)
        {
            foreach (Element blueSquare in bluePlotSquare)
            {
                if (blueSquare.ModelReference == model)
                {
                    return blueSquare;
                }
            }
            return null;
        }
        public static Attachment GetBorderAttachment(ModelReference model)
        {
            Attachment borderAttachment = null;
            foreach (Attachment attachment in model.Attachments)
            {
                if (attachment.AttachModelName == "Default")
                {
                    borderAttachment = attachment;
                }
            }
            return borderAttachment;
        }

        public static void DeleteNonSheetElements(ref ModelReference model,List<Element> copiedInElements)
        {
            foreach (Element element in copiedInElements)
            {
                if (element.Level != null)
                {
                    if (element.Level.Name != "DsnShtPlotShape" || !element.IsShapeElement())
                    {
                        model.RemoveElement(element);
                    }
                }
                else
                {
                    model.RemoveElement(element);
                }
            }
        }
        public static Element GetRedPlotInModelReference(ModelReference model,List<Element> redPlotSquare,ShapeElement blueSquare)
        {
            foreach (Element redSquare in redPlotSquare)
            {
                if (redSquare.ModelReference == model)
                {
                    if (redSquare.IsShapeElement())
                    {
                        if ((int)redSquare.AsShapeElement().GetVertices()[0].X != (int)blueSquare.GetVertices()[0].X)
                        {
                            return redSquare;
                        }
                    }
                }
            }
            return null;
        }
        public static Tuple<ShapeElement,ShapeElement> GetPlotShapes(ModelReference model)
        {
            IGetActiveScannable scannableGetter = new ModelAttachment(model);
            IGetElementsOnLevel elementsOnLevel = new GetElementsByLevel(scannableGetter);
            IGetLevelByName levelGetter = new GetLevelByName();
            Level level = levelGetter.GetLevelFromString("dsnShtPlotShapeBatchPrint");
            List<Element> bluePlotSquares = elementsOnLevel.GetterByLevel(level);
            Element blueSquare = GetPlotInModelReference(model,bluePlotSquares);
            //blueSquare.IsHighlighted = true;

            Attachment borderAttachment = GetBorderAttachment(model);

            ICopyInAttachment attachmentCopier = new AttachmentIntoActiveModelCopier(new Scannable(borderAttachment));
            List<Element> copiedInElements = attachmentCopier.CopyInAttachment();
            DeleteNonSheetElements(ref model,copiedInElements);

            levelGetter = new GetLevelByName();
            level = levelGetter.GetLevelFromString("DsnShtPlotShape");
            List<Element> redPlotSquares = elementsOnLevel.GetterByLevel(level);
            Element redSquare = GetRedPlotInModelReference(model,redPlotSquares,blueSquare.AsShapeElement());

            Application app = Utilities.ComApp;
            //redSquare.IsHighlighted = true;
            ShapeElement blueShape = null;
            ShapeElement redShape = null;
            if (blueSquare != null)
            {
                blueShape = blueSquare.AsShapeElement();
            }
            if (redSquare != null)
            {
                redShape = redSquare.AsShapeElement(); 
            }
            return new Tuple<ShapeElement, ShapeElement>(blueShape,redShape);
        }

        public static void Main()
        {
            Application app = Utilities.ComApp;
            foreach (ModelReference model in new List<ModelReference>() { app.ActiveModelReference })//app.ActiveDesignFile.Models
            {
                if (model.Name.Contains("78029115J"))
                {
                    int number = Convert.ToInt32(model.Name.Replace("78029115J", ""));
                    if (number > 73)
                    {
                        Tuple<ShapeElement, ShapeElement> squares = GetPlotShapes(app.ActiveModelReference);
                        if (squares.Item1 != null && squares.Item2 != null)
                        {
                            ShapeElement blueSquare = squares.Item1;
                            ShapeElement redSquare = squares.Item2;

                            Point3d[] bv = blueSquare.GetVertices();
                            Point3d[] vertices = redSquare.GetVertices();
                            ShapeElement newShape = app.CreateShapeElement1(blueSquare, vertices);
                            model.AddElement(newShape);
                            model.RemoveElement(redSquare);
                            model.RemoveElement(blueSquare);      
                        }
                    }
                }
            }
        }
    }
}