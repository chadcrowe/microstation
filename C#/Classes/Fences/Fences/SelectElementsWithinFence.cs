﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;

namespace Fences
{
    namespace FencedElements
    {
        interface SelectWithinFenceByLevel
        {
            void SelectAllElementsWithinActiveFenceByLevel();
        }
        abstract public class SelectElementsWithinFenceByLevelSetup
        {
            protected Application app;
            protected Level level;
            protected ModelReference model;
            protected ElementEnumerator ee;
            protected Element element;
            protected Fence fence;
            public SelectElementsWithinFenceByLevelSetup(Level level)
            {
                this.app = Utilities.ComApp;
                this.level = level;
                this.model = app.ActiveModelReference;
                this.element = null;
                this.fence = app.ActiveDesignFile.Fence;
                this.ee = fence.GetContents();
            }
        }
        public class SelectElementsWithinFenceByLevel : SelectElementsWithinFenceByLevelSetup, SelectWithinFenceByLevel
        {
            public SelectElementsWithinFenceByLevel(Level level):base(level)
            {
            }
            public void SelectAllElementsWithinActiveFenceByLevel()
            {
                while (this.ee.MoveNext())
                {
                    this.element = this.ee.Current;
                    if (this.element.IsGraphical)
                    {
                        if (this.element.Level != null)
                        {
                            if (this.element.Level.Name == this.level.Name)
                            {
                                this.model.SelectElement(this.element);
                            }
                        }
                    }
                }
            }
        }
    }
}
