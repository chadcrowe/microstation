﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using MSB = Bentley.Interop.MicroStationDGN;

namespace Scaler
{
    public class ScaleSelectedTextNodes
    {
        MSB.Application app;
        public ScaleSelectedTextNodes()
        {
            app = Utilities.ComApp;
        }
        public void ScaleHighlightedTextNodes()
        {
            ElementEnumerator ee = app.ActiveModelReference.Scan();
            while (ee.MoveNext())
            {
                Element e = ee.Current;
                if (e.IsGraphical)
                {
                    if (e.IsHighlighted)
                    {
                        if (e.IsTextNodeElement())
                        {
                            TextNodeElement textNode = e.AsTextNodeElement();
                            double scale = 5/GetTextNodeSize(textNode);
                            textNode.ScaleAll(textNode.Origin, scale, scale,scale);
                            textNode.Rewrite();
                        }
                    }
                }
            }
        }
        private double GetTextNodeSize(TextNodeElement textNode)
        {
            ElementEnumerator ee = textNode.GetSubElements();
            while (ee.MoveNext())
            {
                Element element = ee.Current;
                if(element.IsTextElement()){
                    TextElement textElement = element.AsTextElement();
                    double size = textElement.TextStyle.Height;
                    return size;
                }
            }
            return 1;
        }
    }
}
