﻿using System;
using System.Collections.Generic;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using Readers;
namespace Deleters
{
    public abstract class DeleteDuplicates
    {
        protected Application app;
        protected List<TextNodeElement> toBeRemoved = new List<TextNodeElement>();
        public DeleteDuplicates()
        {
            app = Utilities.ComApp;
        }
        protected abstract void DeleteDuplicateTextNodes(List<TextNodeElement> textNodeList, TextNodeReader TnReader);
        protected void DoRemovals()
        {
            foreach (var element in toBeRemoved)
            {
                app.ActiveModelReference.RemoveElement(element);
            }
        }
    }
    public sealed class DeleteDuplicateTextNode : DeleteDuplicates
    {
        public DeleteDuplicateTextNode(List<TextNodeElement> textNodeList,TextNodeReader TnReader)
            : base()
        {
            this.DeleteDuplicateTextNodes(textNodeList, TnReader);
        }
        protected override void DeleteDuplicateTextNodes(List<TextNodeElement> textNodeList,TextNodeReader TnReader)
        {
            foreach (var outerTextnode in textNodeList)
            {
                if (!toBeRemoved.Contains(outerTextnode))
                {
                    foreach (var innerTextnode in textNodeList)
                    {
                        if (outerTextnode != innerTextnode)
                        {
                            if (outerTextnode.Origin.X == innerTextnode.Origin.X &&
                                outerTextnode.Origin.Y == innerTextnode.Origin.Y)
                            {
                                
                                if (TnReader.GetRawText(outerTextnode) == TnReader.GetRawText(innerTextnode))
                                {
                                    innerTextnode.Color = 1;
                                    innerTextnode.Rewrite();
                                    toBeRemoved.Add(innerTextnode);
                                    //app.ActiveModelReference.RemoveElement(innerTextnode);
                                }
                            }
                        }
                    }
                }
            }
            DoRemovals();
        }
    }
}