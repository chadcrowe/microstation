﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bentley.Interop.MicroStationDGN;

namespace Records
{
    public class Extractor : IExtractTextNodes , IExtractLineLength
    {
        protected CellElement Cell;
        protected double LineElementLength;
        protected List<TextNodeElement> TextNodes = new List<TextNodeElement>();
        public Extractor(CellElement cell)
        {
            this.Cell = cell;
        }

        public List<TextNodeElement> Extract()
        {
            ElementEnumerator cellEe = Cell.GetSubElements();
            while (cellEe.MoveNext())
            {
                Element groupElement = cellEe.Current;
                if (groupElement.IsCellElement())
                {
                    ElementEnumerator cellEnumerator = groupElement.AsCellElement().GetSubElements();
                    while (cellEnumerator.MoveNext())
                    {
                        Element element = cellEnumerator.Current;
                        string type = element.Type.ToString();
                        if (element.IsTextElement())
                        {
                            MessageBox.Show(element.AsTextElement().Text);
                        }
                        if (element.IsTextNodeElement())
                        {
                            TextNodes.Add(element.AsTextNodeElement());
                        }
                        if (element.IsLineElement())
                        {
                            this.LineElementLength = element.AsLineElement().Length;
                        }
                    }
                }
            }
            return TextNodes;
        }

        public double ExtractLineLength()
        {
            return this.LineElementLength;
        }
    }
}

namespace Records
{
    public interface IExtractTextNodes
    {
        List<TextNodeElement> Extract();
    }

    public interface IExtractLineLength
    {
        double ExtractLineLength();
    }
}