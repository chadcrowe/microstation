﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Records
{
    public class Parser : IParseLineStrings
    {
        protected string Length = "";
        protected string Label = "";
        protected string LevelName = "";
        protected List<string> Station = new List<string>();

        protected List<string> StationDelimiters;
        protected List<string> LabelDelimiters;
        protected List<string> LengthDelimiters;
        protected List<string> LevelDelimiters;
        protected List<string> LineStrings;
        public Parser(List<string> lineStrings, List<string> stationDelimiter, List<string> labelDelimiter, List<string> lengthDelimiter,List<string> levelDelimiter)
        {
            this.LineStrings = lineStrings;
            this.StationDelimiters = stationDelimiter;
            this.LabelDelimiters = labelDelimiter;
            this.LengthDelimiters = lengthDelimiter;
            this.LevelDelimiters = levelDelimiter;
            Parse();
        }
        protected void Parse()
        {
            foreach (var line in LineStrings)
            {
                foreach (var delimiter in StationDelimiters)
                {
                    if (line.Contains(delimiter))
                    {
                        this.Station.Add(line);
                    }
                }
                foreach (var delimiter in LabelDelimiters)
                {
                    if (line.Contains(delimiter))
                    {
                        this.Label = line;
                    }
                }
                foreach (var delimiter in LengthDelimiters)
                {
                    if (line.Contains(delimiter))
                    {
                        this.Length = line;
                    }
                }
                foreach (var delimiter in LevelDelimiters)
                {
                    if (line.Contains(delimiter))
                    {
                        this.LevelName = line;
                    }
                }
            }
        }

        public QuantityRecord GetRecord()
        {
            QuantityRecord note = new QuantityRecord(this.Station, this.Label, this.LevelName, this.Length);
            return note;
        }

    }
}

namespace Records
{
    public interface IParseLineStrings
    {
        QuantityRecord GetRecord();
    }
}