﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;
using Readers;
using Scanners.Group;
using Attachments;
using Bentley.General.Excel;
using Excel = Microsoft.Office.Interop.Excel;

namespace Records
{
    public class QuantityRecord
    {
        public List<string> Station;
        public string Label;
        public string LevelName;
        public string Length;
        public QuantityRecord(List<string> station, string label, string level, string length)
        {
            this.Station = station;
            this.Label = label;
            this.LevelName = level;
            this.Length = length;
        }
    }

    public class Quantity
    {
        public QuantityRecord BeginRecord;
        public QuantityRecord EndRecord;
        public double LineLength;

        public Quantity(QuantityRecord beginRecord,QuantityRecord endRecord,double lineLength)
        {
            this.BeginRecord = beginRecord;
            this.EndRecord = endRecord;
            this.LineLength = lineLength;
        }
    }

    public class QuantityBuilder : IQuantityBuilder
    {
        protected List<Quantity> AggregateQuantities = new List<Quantity>();
        protected List<string> StationDelimiters = new List<string>();
        protected List<string> LabelDelimiters = new List<string>();
        protected List<string> LengthDelimiters = new List<string>();
        protected List<string> LevelDelimiters = new List<string>();
        public QuantityBuilder(string stationDelimiter,List<string> labelDelimiters,string lengthDelimiter,string levelDelimiter)
        {
            StationDelimiters.Add(stationDelimiter);
            foreach (var labelDelimiter in labelDelimiters)
            {
                LabelDelimiters.Add(labelDelimiter);
            }
            LengthDelimiters.Add(lengthDelimiter);
            LevelDelimiters.Add(levelDelimiter);
        }
        public void Build()
        {

            IGetActiveScannable attachmentGetter = new ModelAttachment();
            IFindGroups groupsGetter = new GroupGetter(attachmentGetter);
            List<CellElement> groups = groupsGetter.FindGroups();
            foreach (CellElement cell in groups)
            {
                IExtractTextNodes extractor = new Extractor(cell);
                IExtractLineLength extractLine = new Extractor(cell);
                List<TextNodeElement> textNodes = extractor.Extract();
                double lineLength = extractLine.ExtractLineLength();
                SortedList<string,QuantityRecord> quantities = new SortedList<string,QuantityRecord>();
                foreach (var textNode in textNodes)
                {
                    ITextNodeReader reader = new TextNodeReader(textNode);
                    List<string> lineStrings = reader.GetTextList();

                    IParseLineStrings parser = new Parser(lineStrings, StationDelimiters, LabelDelimiters, LengthDelimiters,LevelDelimiters);
                    QuantityRecord quantity = parser.GetRecord();
                    quantities.Add(quantity.Label,quantity);
                }
                if (textNodes.Count == 2)
                {
                    Quantity record = new Quantity(quantities.ElementAt(0).Value, quantities.ElementAt(1).Value, lineLength);
                    AggregateQuantities.Add(record);   
                }
            }
        }
        public List<Quantity> GetQuantities()
        {
            return AggregateQuantities;
        }
    }

    public class QuantityExcelWriter : IQuantityWriter
    {
        protected int rowCnt = 1;
        protected List<Quantity> Quantities;
        protected Excel.Worksheet Worksheet;
        protected string WorksheetName;
        public QuantityExcelWriter(string worksheetName,List<Quantity> quantities)
        {
            WorksheetName = worksheetName;
            Quantities = quantities;
        }

        public void Write()
        {
            Excel.Application xl = new Excel.Application();
            xl.Visible = true;
            Excel.Workbook wb = xl.Workbooks.Add();
            Worksheet = wb.Sheets[1];
            Worksheet.Name = WorksheetName;
            foreach (var quantity in Quantities)
            {
                WriteToExcel(quantity);
            }
            (Worksheet.Columns[1] as Excel.Range).AutoFit();
            (Worksheet.Columns[2] as Excel.Range).AutoFit();
            (Worksheet.Columns[3] as Excel.Range).AutoFit();
            (Worksheet.Columns[4] as Excel.Range).AutoFit();
            (Worksheet.Columns[5] as Excel.Range).AutoFit();
            string currentDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string savePath = Path.Combine(currentDirectory, WorksheetName + ".xlsx");
            wb.SaveAs(savePath);
            wb.Close();
            xl.Quit();
        }

        protected void WriteToExcel(Quantity quantity)
        {
            WriteRecord(quantity.BeginRecord);
            WriteRecord(quantity.EndRecord);
            rowCnt += 1;
        }

        protected void WriteRecord(QuantityRecord record)
        {
            Worksheet.Range["A" + rowCnt.ToString()].Value2 = record.Label;
            string stationText = "";
            foreach (var station in record.Station)
            {
                stationText += station + " ";
            }
            Worksheet.Range["B" + rowCnt.ToString()].Value2 = stationText;
            Worksheet.Range["C" + rowCnt.ToString()].Value2 = record.Length;
            Worksheet.Range["D" + rowCnt.ToString()].Value2 = record.LevelName;
            rowCnt += 1;
        }
    }
}

namespace Records
{
    public interface IQuantityBuilder
    {
        void Build();
        List<Quantity> GetQuantities();
    }

    public interface IQuantityWriter
    {
        void Write();
    }
}