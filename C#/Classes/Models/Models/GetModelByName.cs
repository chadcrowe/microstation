﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
namespace Models
{
    public class GetModelByName
    {
        private List<ModelReference> models;
        public GetModelByName(List<ModelReference> models)
        {
            this.models = models;
        }
        public ModelReference GetModelFromString(string name)
        {
            foreach (ModelReference model in models)
            {
                if (model.Name == name)
                {
                    return model;
                }
            }
            MessageBox.Show("No model exists with name:" + name);
            return null;
        }
    }
}
