﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using Scanners;

namespace Attachments
{
    public class AttachmentIntoActiveModelCopier : ICopyInAttachment
    {
        protected IScanMe AttachmentToCopy;
        public AttachmentIntoActiveModelCopier(IScanMe scannable)
        {
            AttachmentToCopy = scannable;
        }
        public List<Element> CopyInAttachment()
        {
            Application app = Utilities.ComApp;
            ModelReference model = app.ActiveModelReference;
            List<Element> elementsCopiedIn = new List<Element>();
            ElementEnumerator eE = AttachmentToCopy.Scan();
            while (eE.MoveNext())
            {
                Element element = eE.Current;
                var type = element.Type;
                if (element.IsCellElement())
                {
                    CellElement copiedCell = model.CopyElement(element).AsCellElement();
                    ElementEnumerator cellEe = copiedCell.GetSubElements();
                    model.RemoveElement(copiedCell);
                    while (cellEe.MoveNext())
                    {
                        Element cellSubelement = cellEe.Current;
                        model.AddElement(cellSubelement);
                        elementsCopiedIn.Add(cellSubelement);
                    }
                }
            }
            return elementsCopiedIn;
        }
    }
}

namespace Attachments
{
    public interface ICopyInAttachment
    {
        List<Element> CopyInAttachment();
    }
}