﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;

namespace Models
{
    public class PasteIntoModel
    {
        public PasteIntoModel(ModelReference model, List<Element> pasteElements)
        {
            PasteElementListIntoModel(model, pasteElements);
        }
        private void PasteElementListIntoModel(ModelReference model, List<Element> pasteElements)
        {
            foreach (Element element in pasteElements)
            {
                model.AddElement(element);
            }
        }
    }
}
