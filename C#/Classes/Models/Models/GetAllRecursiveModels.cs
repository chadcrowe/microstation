﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using Scanners;

namespace  Attachments
{
    public class ModelAttachment : ModelSetup
    {
        public ModelAttachment(ModelReference model = null) : base(model) { } //Constructor
        public override void IterateAttachments()
        {
            this.AttachmentList.Add(new Scannable(this.Model));
            foreach (Attachment attachment in this.Model.Attachments)
            {
                this.AttachmentList.Add(new Scannable(attachment));
            }
        }
        public override List<Scannable> GetScannablesWithinModel()
        {
            return this.AttachmentList;
        }
    }    
}

//Abstractions
namespace Attachments
{
    public interface IGetActiveScannable
    {
        void IterateAttachments();
        List<Scannable> GetScannablesWithinModel();
    }
    public abstract class ModelSetup : IGetActiveScannable
    {
        protected Application app;
        protected List<Scannable> AttachmentList = new List<Scannable>();
        protected ModelReference Model;
        public ModelSetup(ModelReference model = null)
        {
            app = Utilities.ComApp;
            if (model == null)
            {
                this.Model = app.ActiveModelReference;
            }
            else
            {
                this.Model = model;
            }
        }
        public abstract void IterateAttachments();
        public abstract List<Scannable> GetScannablesWithinModel();
    }
}
