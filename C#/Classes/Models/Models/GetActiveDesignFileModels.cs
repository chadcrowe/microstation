﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;

namespace Models
{
    public class GetActiveDesignFileModels
    {
        Bentley.Interop.MicroStationDGN.Application app = Utilities.ComApp;
        public GetActiveDesignFileModels()
        {
        }
        public List<ModelReference> GetListOfModels()
        {
            List<ModelReference> models = new List<ModelReference>();
            foreach (ModelReference model in app.ActiveDesignFile.Models)
            {
                models.Add(model);
            }
            return models;
        }
    }
}
