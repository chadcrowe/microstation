﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;
using Readers;

namespace Selectors
{
    namespace TextNodeList
    {
        namespace MatchString
        {
            public abstract class HighlighterSetup
            {
                protected ModelReference model;
                protected Application app;
                protected ElementEnumerator ee;
                protected TextNodeReader reader;
                protected string textIWant;
                protected List<TextNodeElement> textNodeList;
                public HighlighterSetup(ModelReference model, TextNodeReader reader,List<TextNodeElement> textNodeList, string textIWant)
                {
                    app = Utilities.ComApp;
                    if (model == null)
                    {
                        this.model = app.ActiveModelReference;
                    }
                    else
                    {
                        this.model = model;
                    }
                    ee = this.model.Scan();
                    this.reader = reader;
                    this.textIWant = textIWant;
                    this.textNodeList = textNodeList;
                }
                abstract protected void HighlightMatchingText();
            }
            public class Highlighter : HighlighterSetup
            {
                public Highlighter(ModelReference model, TextNodeReader reader, List<TextNodeElement> textNodeList, string textIWant)
                    : base(model, reader, textNodeList, textIWant)
                {
                    HighlightMatchingText();
                }
                protected override void HighlightMatchingText()
                {
                    foreach (TextNodeElement textNode in this.textNodeList)
                    {
                        TextNodeReader reader = new TextNodeReader(textNode);
                        string text = reader.GetRawText();
                        if (text.Contains(this.textIWant))
                        {
                            this.model.SelectElement(textNode);
                        }
                    }

                }
            }


        }
    }
}

