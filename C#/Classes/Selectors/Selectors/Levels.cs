﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;

namespace Selectors
{
    namespace Levels
    {
        namespace ByName
        {
            public class LevelHighlighter : IHighlightLevels
            {
                protected string Name;
                public LevelHighlighter(string name)
                {
                    this.Name = name;
                }
                public void HighlightLevels()
                {
                    Application app = Utilities.ComApp;
                    ModelReference model = app.ActiveModelReference;
                    ElementEnumerator eE = model.Scan();
                    while (eE.MoveNext())
                    {
                        Element element = eE.Current;
                        if (element.IsGraphical)
                        {
                            if (element.Level != null)
                            {
                                if (element.Level.Name == this.Name)
                                {
                                    element.IsHighlighted = true;
                                }
                            }
                        }
                    }
                }

            }
        }
    }
}

//interface
namespace Selectors
{
    namespace Levels
    {
        namespace ByName
        {
            interface IHighlightLevels
            {
                void HighlightLevels();
            }
        }
    }
}