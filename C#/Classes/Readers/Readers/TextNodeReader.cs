﻿using System;
using System.Collections.Generic;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;

namespace Readers
{
    public class TextNodeReader : ITextNodeReader
    {
        protected TextNodeElement TextNode;
        protected ElementEnumerator Enumerator;
        protected List<string> TextLineList;
        public TextNodeReader(TextNodeElement textNode)
        {
            this.TextNode = textNode;
            this.Enumerator = this.TextNode.GetSubElements();
        }

        public void CreateTextList()
        {
            TextLineList = new List<string>();
            while (Enumerator.MoveNext())
            {
                TextElement textElement = Enumerator.Current.AsTextElement();
                TextLineList.Add(textElement.Text);
            }
        }
        public List<string> GetTextList()
        {
            if (this.TextLineList != null)
            {
                return this.TextLineList;
            }
            else
            {
                CreateTextList();
                return GetTextList();
            }
        }
        public string GetRawText()
        {
            string text = "";
            while (Enumerator.MoveNext())
            {
                TextElement textElement = Enumerator.Current.AsTextElement();
                text += textElement.Text;
            }
            return text;
        }
    }
}

namespace Readers
{
    public interface ITextNodeReader
    {
        string GetRawText();
        List<string> GetTextList();
    }
}