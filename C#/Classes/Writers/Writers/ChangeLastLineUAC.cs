﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;
using System.Windows.Forms;

namespace Writers
{
    namespace TextNodes
    {
        namespace Style
        {
            abstract public class StyleLinesSetup
            {
                protected BNS.Application app;
                protected ModelReference model;
                protected ElementEnumerator ee;
                protected Element element;
                protected TextNodeElement textnode;
                protected Level level;
                protected string stringToFind;
                public StyleLinesSetup(TextNodeElement textnode, string stringToFind, Level level = null)
                {
                    this.app = Utilities.ComApp;
                    this.model = this.app.ActiveModelReference;
                    this.ee = textnode.GetSubElements();
                    this.textnode = textnode;
                    this.level = level;
                    this.stringToFind = stringToFind;
                }
                abstract protected bool DoesTextElementContain(string looking4, TextElement textElement);
                abstract protected void MakeStyleChanges(TextElement textElement);
                abstract protected void ChangeUACTextElement();
            }
            public sealed class StyleLines : StyleLinesSetup
            {
                public StyleLines(TextNodeElement highlightedTextnode, string stringToFind, Level level)
                    : base(highlightedTextnode, stringToFind, level)
                {
                    ChangeUACTextElement();
                }
                protected override void ChangeUACTextElement()
                {
                    while (this.ee.MoveNext())
                    {
                        this.element = ee.Current;
                        if (this.element.IsTextElement())
                        {
                            TextElement textElement = this.element.AsTextElement();
                            if (DoesTextElementContain(stringToFind, textElement))
                            {
                                this.MakeStyleChanges(textElement);
                            }
                        }
                    }
                }
                protected override bool DoesTextElementContain(string looking4, TextElement textElement)
                {
                    if (textElement.Text.Contains(looking4))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                protected override void MakeStyleChanges(TextElement textElement)
                {
                    textElement.TextStyle.Color = 31;
                    textElement.TextStyle.Height = 5;
                    textElement.TextStyle.Width = 5;
                    if (this.level != null)
                    {
                        textElement.Level = level;
                    }
                    textElement.LineWeight = 4;
                    textElement.Rewrite();
                }
            }
        }
    }
}