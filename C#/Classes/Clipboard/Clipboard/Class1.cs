﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;
using Scanners.Element.Highlighted;
using System.Windows.Forms;

namespace Clipboard
{
    namespace Highlighted
    {
        public class CopyHighlightedElement : SetClipboardAttributes
        {
            public CopyHighlightedElement(IFindHighlightedElements highlightedElementList) : base(highlightedElementList){} //Constructor
            public override void SetClipBoardToHighlightedElementLevelString()
            {
                foreach (var element in this.HighlightedElementsGetter.GetHighlightedElementList())
                {
                    if (element.Level != null)
                    {
                        string levelName = element.Level.Name;
                        System.Windows.Forms.Clipboard.SetText(levelName);
                    }
                }
            }
        }   
    }
}

//Abstractions
namespace Clipboard
{
    namespace Highlighted
    {
        public interface ISetClipboard
        {
            void SetClipBoardToHighlightedElementLevelString();
        }

        public abstract class SetClipboardAttributes : ISetClipboard
        {
            protected IFindHighlightedElements HighlightedElementsGetter;
            public SetClipboardAttributes(IFindHighlightedElements highlightedElementList)
            {
                this.HighlightedElementsGetter = highlightedElementList;
                this.HighlightedElementsGetter.RecursivelySearchForHighlightedElements();
            }
            public abstract void SetClipBoardToHighlightedElementLevelString();
        }
    }
}