﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using MSB = Bentley.Interop.MicroStationDGN;

namespace Coverters
{
    namespace TextNode
    {
        public class ToTextNode
        {
            public ToTextNode()
            {
            }
            public List<TextNodeElement> Convert(List<Element> elements)
            {
                List<TextNodeElement> textNodeList = new List<TextNodeElement>();
                foreach (var element in elements)
                {
                    if (element.IsTextNodeElement())
                    {
                        textNodeList.Add(element.AsTextNodeElement());
                    }
                }
                return textNodeList;
            }
        }
    }
}
