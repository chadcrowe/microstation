﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;
namespace Levels
{
    class LevelHider : IHideLevel
    {
        public LevelHider()
        {
            
        }
        public void HideLevel(Level level,ModelReference model = null)
        {
            if (model == null)
            {
                Application app = Utilities.ComApp;
                model = app.ActiveModelReference;
            }
            model.Levels[level.Name].IsDisplayed = false;
        }
    }
}

namespace Levels
{
    interface IHideLevel
    {
        void HideLevel(Level level, ModelReference model = null);
    }
}