﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using MSapp = Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using System.Windows.Forms;

namespace Levels
{
    public sealed class GetLevelByName : IGetLevelByName
    {
        private MSapp.Application app;
        public GetLevelByName()
        {
            this.app = Utilities.ComApp;
        }
        public Level GetLevelFromString(string name) 
        {
            try
            {
                Level level = app.ActiveDesignFile.Levels[name];
                return level;
            }
            catch (Exception e)
            {
                MessageBox.Show("Level Name does not exist:" + name);
                return null;
            }
        }
    }
}

namespace Levels
{
    public interface IGetLevelByName
    {
        Level GetLevelFromString(string name);
    }
}