﻿using System.Collections.Generic;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;
using System;
using Attachments;

namespace Scanners
{
    namespace Element
    {
        namespace Getter
        {
            namespace Collection
            {
                public class GetElementsByLevel : GetElementsByLevelSetup, IGetElementsOnLevel
                {
                    public GetElementsByLevel(IGetActiveScannable models)
                        : base(models)
                    {

                    }
                    public List<BNS.Element> GetterByLevel(Level level)
                    {
                        elementList = new List<BNS.Element>();
                        foreach (IScanMe scannable in Models.GetScannablesWithinModel())
                        {
                            ElementEnumerator ee = scannable.Scan();
                            while (ee.MoveNext())
                            {
                                BNS.Element element = ee.Current;
                                if (element.IsGraphical)
                                {
                                    if (element.Level != null)
                                    {
                                        if (element.Level.Name == level.Name)
                                        {
                                            elementList.Add(element);
                                        }
                                    }
                                }
                            }   
                        }
                        return elementList;
                    }
                }

            }
        }
    }
}

namespace Scanners
{
    namespace Element
    {
        namespace Getter
        {
            namespace Collection
            {
                public interface IGetElementsOnLevel
                {
                    List<BNS.Element> GetterByLevel(Level level);
                }
                abstract public class GetElementsByLevelSetup
                {
                    protected Application app;
                    protected List<BNS.Element> elementList;
                    protected IGetActiveScannable Models;
                    public GetElementsByLevelSetup(IGetActiveScannable models)
                    {
                        app = Utilities.ComApp;
                        Models = models;
                        Models.IterateAttachments();
                    }
                }
            }
        }
    }
}