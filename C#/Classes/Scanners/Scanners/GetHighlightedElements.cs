﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;
using Attachments;

namespace Scanners
{
    namespace Element
    {
        namespace Highlighted
        {
            public class GetHighlightedElements : GetHighlightedElementsAttributes
            {
                public GetHighlightedElements(IGetActiveScannable allAttachments, DesignFile designFile = null)
                    : base()
                {
                    this.App = Utilities.ComApp;
                    this.ElementList = new List<BNS.Element>();
                    if (designFile == null)
                    {
                        designFile = App.ActiveDesignFile;
                    }
                    allAttachments.IterateAttachments();
                    this.ScannablesList = allAttachments.GetScannablesWithinModel();
                }

                public override void RecursivelySearchForHighlightedElements()
                {
                    foreach (IScanMe model in this.ScannablesList)
                    {
                        ElementEnumerator Ee = model.Scan();
                        ScanElements(Ee);
                    }
                }

                protected void ScanElements(ElementEnumerator Ee)
                {
                    while (Ee.MoveNext())
                    {
                        BNS.Element element = Ee.Current;
                        if (element.IsGraphical)
                        {
                            if (element.IsHighlighted)
                            {
                                this.ElementList.Add(element);
                            }
                        }
                    }
                }

                protected void CheckActiveModelReference()
                {
                    ElementEnumerator Ee = this.App.ActiveModelReference.Scan();
                    ScanElements(Ee);
                }
                public override List<BNS.Element> GetHighlightedElementList()
                {
                    return this.ElementList;
                }
            }
        }
    }
}

//abstract
namespace Scanners
{
    namespace Element
    {
        namespace Highlighted
        {
            public interface IFindHighlightedElements
            {
                void RecursivelySearchForHighlightedElements();
                List<BNS.Element> GetHighlightedElementList();
            }
            public abstract class GetHighlightedElementsAttributes : IFindHighlightedElements
            {
                protected Application App;
                protected List<BNS.Element> ElementList;
                protected List<Scannable> ScannablesList;
                protected GetHighlightedElementsAttributes()
                {
                    App = Utilities.ComApp;
                }
                public abstract void RecursivelySearchForHighlightedElements();
                public abstract List<BNS.Element> GetHighlightedElementList();
            }
        }
    }
}