﻿using System;
using System.Collections.Generic;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;

namespace Scanners
{
    namespace TextNode
    {
        namespace NotHighlighted
        {
            interface GetNodes{
                List<BNS.Element> GetTextNodesOnLevel(Level level);
            }
            public abstract class TextNodeScannerSetup
            {
                protected BNS.Application bApp = Utilities.ComApp;
                protected ElementEnumerator ee;
                protected List<BNS.Element> textNodeList;
                protected ModelReference model;
                public TextNodeScannerSetup(ModelReference model = null)
            {
                if (model == null)
                {
                    this.model = this.bApp.ActiveModelReference;
                }
                this.ee = this.model.Scan();
            }
            }
            public sealed class TextNodeScanner : TextNodeScannerSetup, GetNodes
            {

                public TextNodeScanner(ModelReference model):base(model)
                {
                    //empty constructor
                }
                public List<BNS.Element> GetTextNodesOnLevel(Level level)
                {
                    textNodeList = new List<BNS.Element>();
                    while (ee.MoveNext())
                    {
                        BNS.Element element = ee.Current;
                        if (element.IsGraphical)
                        {
                            if (element.Level != null)
                            {
                                if (element.Level.Name == level.Name)
                                {
                                    if (element.IsTextNodeElement())
                                    {
                                        textNodeList.Add(element);
                                    }
                                }
                            }
                        }
                    }
                    return textNodeList;
                }

            }
        }
    }
}
