﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using BNS = Bentley.Interop.MicroStationDGN;

namespace Scanners
{
    public class Scannable : IScanMe
    {
        protected ModelReference ModelReference;
        protected Attachment Attachment;
        public Scannable(ModelReference scannableObject)
        {
            this.ModelReference = scannableObject;
        }
        public Scannable(Attachment scannableObject)
        {
            this.Attachment = scannableObject;
        }

        public ElementEnumerator Scan()
        {
            ElementEnumerator eE = null;
            if (this.ModelReference != null)
            {
                eE = this.ModelReference.Scan();
            }
            if (this.Attachment != null)
            {
                eE = this.Attachment.Scan();
            }
            return eE;
        }
    }
}


namespace Scanners
{
    public interface IScanMe
    {
        ElementEnumerator Scan();
    }
}
