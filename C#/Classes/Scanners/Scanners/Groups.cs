﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Attachments;
using Bentley.Internal.MicroStation.Elements;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using Readers;
using Records;
using BNS = Bentley.Interop.MicroStationDGN;

namespace Scanners
{
    namespace Group
    {
        public class GroupGetter : Groups
        {
            public GroupGetter(IGetActiveScannable attachments) : base(attachments) { }

            public override List<CellElement> FindGroups()
            {
                BNS.Application app = Utilities.ComApp;
                ModelReference model =  app.ActiveModelReference;
                ElementEnumerator eE = model.Scan();
                while (eE.MoveNext())
                {
                    BNS.Element element = eE.Current;
                    if (element.IsCellElement())
                    {
                        if (!element.IsHidden)
                        {
                            CellElement cell = element.AsCellElement();
                            GroupElements.Add(cell);
                        }
                    }
                }
                return this.GroupElements;
            }
        }
    }
}
namespace Scanners
{
    namespace Group
    {
        public interface IFindGroups
        {
            List<CellElement> FindGroups();
        }
        public abstract class Groups : IFindGroups
        {
            protected List<Scannable> Attachments;
            protected List<CellElement> GroupElements = new List<CellElement>();
            public Groups(IGetActiveScannable attachments)
            {
                attachments.IterateAttachments();
                this.Attachments = attachments.GetScannablesWithinModel();
            }

            public abstract List<CellElement> FindGroups();
        }
    }
}