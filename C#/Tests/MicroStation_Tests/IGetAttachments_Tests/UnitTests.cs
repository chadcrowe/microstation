﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bentley.Interop.MicroStationDGN;
using Bentley.MicroStation.InteropServices;
using Attachments;
using System.Collections.Generic;
using System.Windows.Forms;
using Scanners.Element.Highlighted;
using Application = Bentley.Interop.MicroStationDGN.Application;
using Clipboard.Highlighted;
using Clipboard;
using Clipboard = System.Windows.Forms.Clipboard;
using Selectors.Levels.ByName;

namespace IGetAttachments_Tests
{
    [TestClass]
    internal sealed class UnitTests : Bentley.MicroStation.AddIn
    {
        private UnitTests(System.IntPtr mdlDesc)
            : base(mdlDesc)
        {
        }
        
        protected override int Run(string[] commandLine)
        {
            MessageBox.Show("Starting Tests");
            IGetAttachments_Tests();
            IFindHighlightedElements_Tests();
            CopyToClipboard_Tests();
            TestLevelHighlighter();
            MessageBox.Show("Finished Testing");
            return 0;
        }
        [TestMethod]
        private void IGetAttachments_Tests()
        {
            IGetAttachments modelGetter = new ModelAttachment(null);
            modelGetter.IterateAttachments();
            List<Attachment> attachments = modelGetter.GetAttachments();
            Assert.IsTrue(attachments.Count == 2);
        }

        private void SelectTwoElements(Application app)
        {
            int elementCounter = 0;
            ModelReference model = app.ActiveModelReference;
            ElementEnumerator ee = model.Scan();
            while (ee.MoveNext() && elementCounter  < 2)
            {
                Element element = ee.Current;
                if (element.IsGraphical)
                {
                    element.IsHighlighted = true;
                    elementCounter += 1;
                }
            }
        }
        private void UnselectAllElements(Application app)
        {
            ModelReference model = app.ActiveModelReference;
            ElementEnumerator ee = model.Scan();
            while (ee.MoveNext())
            {
                Element element = ee.Current;
                if (element.IsGraphical)
                {
                    element.IsHighlighted = false;
                }
            }
        }
        [TestMethod]
        private void IFindHighlightedElements_Tests()
        {
            Application app = Utilities.ComApp;
            IGetAttachments modelGetter = new ModelAttachment(null);
            modelGetter.IterateAttachments();
            SelectTwoElements(app);
            IFindHighlightedElements finder = new GetHighlightedElements(modelGetter, null);
            finder.RecursivelySearchForHighlightedElements();
            List<Element> highlighted =  finder.GetHighlightedElementList();
            Assert.IsTrue(highlighted.Count == 2);
            UnselectAllElements(app);
        }
        private void SelectLine(Application app)
        {
            ModelReference model = app.ActiveModelReference;
            ElementEnumerator ee = model.Scan();
            while (ee.MoveNext())
            {
                Element element = ee.Current;
                if (element.IsGraphical)
                {
                    if (element.IsLineElement())
                    {
                        if (Convert.ToInt32(element.AsLineElement().Length) == 1)
                        {
                            element.IsHighlighted = true;
                        }
                    }
                }
            }
        }
        [TestMethod]
        private void CopyToClipboard_Tests()
        {
            Application app = Utilities.ComApp;
            IGetAttachments modelGetter = new ModelAttachment(null);
            modelGetter.IterateAttachments();
            SelectLine(app);
            IFindHighlightedElements finder = new GetHighlightedElements(modelGetter, null);
            finder.RecursivelySearchForHighlightedElements();
            List<Element> highlighted = finder.GetHighlightedElementList();
            CopyHighlightedElement copyToClipboard = new CopyHighlightedElement(finder);
            copyToClipboard.SetClipBoardToHighlightedElementLevelString();
            string text = System.Windows.Forms.Clipboard.GetText();
            Assert.IsTrue(System.Windows.Forms.Clipboard.GetText() == "North Arrow");
            UnselectAllElements(app);
        }

        private LineElement CreateLine(Application app)
        {
            Point3d[] points = new Point3d[2];
            Point3d firstPoint = new Point3d();
            firstPoint.X = 0;
            firstPoint.Y = 0;
            firstPoint.Z = 0;
            Point3d secondPoint = new Point3d();
            secondPoint.X = 10;
            secondPoint.Y = 10;
            secondPoint.Z = 10;
            LineElement line = app.CreateLineElement1(null, points);
            return line;
        }

        private int GetHighlightCount(string levelName)
        {
            LevelHighlighter highlighter = new LevelHighlighter(levelName);
            highlighter.HighlightLevels();
            IGetAttachments modelGetter = new ModelAttachment(null);
            modelGetter.IterateAttachments();
            IFindHighlightedElements finder = new GetHighlightedElements(modelGetter, null);
            finder.RecursivelySearchForHighlightedElements();
            List<Element> highlighted = finder.GetHighlightedElementList();
            return highlighted.Count;
        }
        [TestMethod]
        private void TestLevelHighlighter()
        {
            Application app = Utilities.ComApp;
            Level level = app.ActiveDesignFile.Levels[1];
            int firstCount = GetHighlightCount(level.Name);

            LineElement line = CreateLine(app);
            line.Level = level;
            app.ActiveModelReference.AddElement(line);

            LevelHighlighter highlighter = new LevelHighlighter(level.Name);
            highlighter.HighlightLevels();

            int secondCount = GetHighlightCount(level.Name);
            Assert.IsTrue(secondCount == (firstCount + 1));
        }
    }
}